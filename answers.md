# P2 answer

## Merge Comments

Comment on the differences between your `p1_results` and the ones given for this practical.
Il n'y avait aucune différence.

## Result Comments

Are your results identical to the ones demanded in **practical 2**? If not, why?
Après avoir ordonné le premier changement de position (qui se déroule correctement), les suivants se font instantanéments.
Le problème a lieu dans le fichier *position_interpolation.py*: la condition t > Dt est toujours vraie, bien que cela ne devrait pas être le cas.

# P3 answer

## Merge Comments
Après le merge, le programme fonctionne correctement.

## Result Comments
Mon ordinateur ne reconnaissait pas `std_msgs/msg/Bool`, j'ai donc utilisé `example_interfaces/msg/Bool`. Hormis cela, tout fonctionne correctement.


# P5 answer

## Merge Comments

## Result Comments
Les nodes, subscriber et publishers fonctionnent, mais le q publié est toujours nul si bien que le robot ne se déplace pas : il reste immobile.

# P6 answer

## Merge Comments
Le programme fonctionne correctemen (Il faudra que je comprenne mon erreur).

## Result Comments
La modélisation fonctionne. Le fichier launch initialise correctement les différentes nodes ainsi que le modèle du bras pour le logiciel.
La node `disturbance` crée bien des perturbations selon $A$ et $w$, paramètre que l'on peut modifier depuis le terminal.