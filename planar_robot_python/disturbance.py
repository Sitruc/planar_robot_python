import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np
from math import sin, pi

class Disturbance(Node):
    def __init__(self):
        super().__init__('disturbance')
        self.__publisher__ = self.create_publisher(JointState, "qdot_disturbance", 10)
        self.declare_parameter('w', 100.0)
        self.declare_parameter('A', 1.0)


        self.tf = 0.1
        self.current_time = 0
        self.create_timer(self.tf, self.timer_callback)


    def timer_callback(self):
        self.current_time+= self.tf
        message = JointState()
        q_dot = np.zeros(2)

        A = self.get_parameter('A').value
        w = self.get_parameter('w').value

        for i in range(2):
            q_dot[i] = A*(i+1)*sin(w*(i+1)*self.current_time)

        message.velocity = q_dot
        self.__publisher__.publish(message)



def main(args=None):
    rclpy.init(args=args)
    disturbance = Disturbance()
    rclpy.spin(disturbance)
    disturbance.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()