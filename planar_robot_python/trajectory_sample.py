import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32
from custom_messages.msg import CartesianState
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np

class TrajectorySample(Node):
    def __init__(self):
        super().__init__('trajectory_sample')
        # TODO: Initialize node to be publisher and subscriber to the necessary topics
        self._publishers_ = self.create_publisher(CartesianState, 'desired_state', 10)
        self._subscribers_ = self.create_subscription(Float32, 'time', self.callback, 10) # Create callback

        # TODO: Initialize main variables
        pi                  = np.array([1.0,2.0])
        pf                  = np.array([3.0,4.0])
        self.pos_interp     = PositionInterpolation(pi,pf,2.0)


    def callback(self, msg): # Il faut le "self"
        # TODO: Function able to compute the position and velocity for a given instant t
        t = msg.data
        pos = self.pos_interp.p(t)
        vel = self.pos_interp.pdot(t)


        message = CartesianState()
        message.x = pos[0]
        message.y = pos[1]
        message.xdot = vel[0]
        message.ydot = vel[1]

        self._publishers_.publish(message)

        self
        # self.get_logger().info("Publishing: x: {x}, y: {y}, xdot: {xdot}, ydot: {ydot}".format(x=pos[0], y=pos[1], xdot=vel[0], ydot=vel[1])) # Equivalent printf


def main(args=None):
    rclpy.init(args=args)
    trajectory_sample = TrajectorySample()
    rclpy.spin(trajectory_sample)
    trajectory_sample.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()