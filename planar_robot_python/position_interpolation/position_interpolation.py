import numpy as np

# Polynomial necessary to the trajectory generation
class FifthOrderPolynomial:
    def __init__(self,pi,pf,Dt):
        self.update(pi,pf,Dt)

    def update(self,pi,pf,Dt):
        self.pi     = pi
        self.pf     = pf
        self.a      = np.array([pi,0.,0.,10. *(pf-pi),-15.*(pf-pi),6.  *(pf-pi)])
        self.Dt     = Dt

    def p(self,t):
        tb      = t/self.Dt
        # print(f"t selon position...: {t}")
        # print(f"Dt:{self.Dt}")
        if (t<=0):
            # print("Temps pas depasse")
            return self.pi
        elif(t>self.Dt):
            # print("Temps depasse")
            return self.pf
        return self.a[0] + self.a[3]*tb**3 + self.a[4]*tb**4 + self.a[5]*tb**5

    def pdot(self,t):
        tb      = t/self.Dt
        if (t<0 or t>self.Dt-0.1):
            return 0.0
        return (3.*self.a[3]*tb**2 + 4.*self.a[4]*tb**3 + 5.*self.a[5]*tb**4) / self.Dt

# Position interpolation
class PositionInterpolation:
    # Create a position interpolator with initial position pi, final position pf and Delta time Dt
    #       The trajectory departs from  pi = pi[0], pi[1]
    #       After Dt seconds, it reaches pf = pf[0], pf[1]    
    def __init__(self,pi,pf,Dt):        
        self.polx   = FifthOrderPolynomial(pi[0],pf[0],Dt)
        self.poly   = FifthOrderPolynomial(pi[1],pf[1],Dt)

    # Regenerate the trajectories for new pi, pf and Dt
    def update(self,pi,pf,Dt):
        self.polx.update(pi[0],pf[0],Dt)
        self.poly.update(pi[1],pf[1],Dt)

    # Delivers the position p = p[0], p[1] for a time t
    def p(self,t):
        return np.array([self.polx.p(t), self.poly.p(t)])
    
    # Delivers the velocity pdot = pdot[0], pdot[1] for a time t
    def pdot(self,t):
        return np.array([self.polx.pdot(t), self.poly.pdot(t)])