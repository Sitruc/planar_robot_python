# Practical 2

Create a node named `trajectory_generator`, complying with the following characteristics:

- Subscribed to a topic `/ref_position`, receiving messages with type `RefPosition`. This type should have the fields `x`, `y` and `deltat`. In order to create new message types, check [this tutorial](https://docs.ros.org/en/iron/Tutorials/Beginner-Client-Libraries/Custom-ROS2-Interfaces.html) and clone [this project](https://gite.lirmm.fr/um_eea_hae806e_practicals/custom_messages) in your `ros_workspace/src` folder. 

- Publisher in a topic named `/desired_state`, in which all the data from the `trajectory_generator` necessary to the controller is streamed. The streamed messages should have type `CartesianState`, having fields `x`, `y`, `xdot` and `ydot`.

- When created, the node should not stream any data, since it does not has access to the initial pose.

- When a first `RefPosition` is received, the note starts to stream this position as desired state.

- When remaining `RefPositions` are received, a trajectory should be streamed taking the previous on as initial pose.

In summary, the expected behavior from this practical is the following:

![complete_gif](../gif/p2_complete.gif)
 ***<p style="text-align: center;">Animation 1: Complete desired behavior.</p>***

The whole pipeline can be tested running `src/planar_robot_python/bash_scripts/p2.sh`, which should lead to the results:

![](../gif/p2.gif)
***<p style="text-align: center;">Animation 2: Desired results when testing.</p>***

The use of the bash scripts `.sh` should be restricted to testing. During the development, it is strongly advised to use  separate commands as shown in *__Animation 1__*.