# Practical 5

## Develop a `controller` node complying with the following characteristics:

- Subscribed to `/desired_state`, in order to read the desired cartesian position and velocity;

- Subscribed to `/kin_data`, in order to read the actual cartesian position and the jacobian matrix;

- Publisher of the desired feedback joint velocities, on topic `/desired_joint_velocities`, of type `sensor_msgs/msg/JointStat`;

- Should publish null desired joint velocities if the node is created before the `/kin_data` topic is active.

## Develop a `simulator` node complying with the following characteristics:

- Subscribed to a topic named `/desired_joint_velocities`, reading the desired joint velocity.

- Streams the next joint positions in `/joint_states`, integrating the variables as $q_{k+1} = q_{k} + dt\,\dot{q}_{desired}$.

In summary, the expected result when running `src/planar_robot_python/bash_scripts/p5.sh` should be the following:

![](../gif/p5.gif)