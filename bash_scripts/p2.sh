. install/setup.sh
ros2 run planar_robot_python trajectory_generator &

sleep 2
echo "\n=============================================================="
echo "[Enter] ros2 topic pub /ref_position custom_messages/msg/RefPosition {x: 1.6, y: 0.0, deltat: 2.0} --once"
read 
ros2 topic pub /ref_position custom_messages/msg/RefPosition "{x: 1.6, y: 0.0, deltat: 2.0}" --once

ros2 run plotjuggler plotjuggler &

sleep 2
echo "\n=============================================================="
echo "------> Load plotjuggler/p2_layout.xml on PlotJuggler"
echo "[Enter] ros2 topic pub /ref_position custom_messages/msg/RefPosition {x: 0.0, y: 1.6, deltat: 2.0} --once"
read 
ros2 topic pub /ref_position custom_messages/msg/RefPosition "{x: 0.0, y: 1.6, deltat: 2.0}" --once

sleep 2
echo "\n=============================================================="
echo "[Enter] ros2 topic pub /ref_position custom_messages/msg/RefPosition {x: -1.6, y: 0.0, deltat: 2.0} --once"
read 
ros2 topic pub /ref_position custom_messages/msg/RefPosition "{x: -1.6, y: 0.0, deltat: 2.0}" --once

sleep 2
echo "\n=============================================================="
echo "[Enter] kill all ROS2 processes"
read 

killall trajectory_generator 
killall plotjuggler